ProjectCar private repo

Team members: 
Mina Maged
Kirolos Lewis
Amal Mohammed
Baraa Gamal
Youssef Hussein

The repo contains:
Lane detector
Obstacle detector
Signs detector

Dependencies to run project:
Python 3.6
openCV
NumPy
SciPy
matplotlib
pickle
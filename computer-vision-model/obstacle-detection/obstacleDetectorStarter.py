import cv2
from obstacleDetectorYolo import *
from imutils.video import FPS
frame_count = 0

vc = cv2.VideoCapture('output1.avi')

fps = FPS().start()
# img = cv2.imwrite('image.jpg',img) #to save image
# displayImage(img,'Original')

if vc.isOpened():
    rval = True
    print('true')
else:
    rval = False
    print('false')

fourcc = cv2.VideoWriter_fourcc(*'XVID')
out = cv2.VideoWriter('output2.avi', fourcc, 25.0, (1280, 720))
while rval == True:
    rval, frame = vc.read()
    if rval == False:
        break
    output = vehicle_detect_yolo(frame)
    frame_count += 1
    print(frame_count)
    #cv2.imshow("test",output)

    out.write(output)
    fps.update()
fps.stop()
print("[INFO] elasped time: {:.2f}".format(fps.elapsed()))
print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))
vc.release()
out.release()




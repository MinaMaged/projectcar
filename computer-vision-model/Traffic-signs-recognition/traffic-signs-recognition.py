import cv2
import numpy as np
import csv
import pickle
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
from tensorflow.contrib.layers import flatten
from sklearn.model_selection import train_test_split
from keras.preprocessing.image import ImageDataGenerator
from sklearn.utils import shuffle
from time import time
import logging, datetime
from skimage.transform import resize
from skimage.filters import gaussian
import pandas as pd
from skimage.exposure import equalize_hist

signnames = pd.read_csv('./signnames.csv')
signnames.set_index('ClassId',inplace=True)
def get_name_from_label(label):
    # Helper, transofrm a numeric label into the corresponding strring
    return signnames.loc[label].SignName

def load_data(training_file,testing_file):
    with open(training_file,mode='rb') as file:
        train = pickle.load(file)
    with open(testing_file,mode='rb') as file:
        test = pickle.load(file)

    return train,test

def preprocess_features(X,equalize_hist=True):
    X = np.array([np.expand_dims(cv2.cvtColor(rgb_img, cv2.COLOR_RGB2YUV)[:, :, 0], 2) for rgb_img in X])

    if equalize_hist:
        X = np.array([np.expand_dims(cv2.equalizeHist(np.uint8(img)), 2) for img in X])
    X = np.float32(X)
    X -= np.mean(X, axis=0)
    X /= (np.std(X, axis=0) + np.finfo('float32').eps)
    return X


img_size = 32


def sharpen_img(img):
    gb = cv2.GaussianBlur(img, (5, 5), 20.0)
    return cv2.addWeighted(img, 2, gb, -1, 0)


def transform_img(img_in):
    img_in = img_in.copy()
    img_out = sharpen_img(img_in)
    img_out = cv2.cvtColor(img_in, cv2.COLOR_RGB2YUV)

    img_out[:, :, 0] = cv2.equalizeHist(img_out[:, :, 0])
    #     img_out[:,:,1] = cv2.equalizeHist(img_out[:,:,1])
    #     img_out[:,:,2] = cv2.equalizeHist(img_out[:,:,2])

    return img_out[:, :, 0]


def random_rotate_img(img):
    c_x, c_y = int(img.shape[0] / 2), int(img.shape[1] / 2)
    ang = 30.0 * np.random.rand() - 15
    Mat = cv2.getRotationMatrix2D((c_x, c_y), ang, 1.0)
    return cv2.warpAffine(img, Mat, img.shape[:2])


def random_scale_img(img):
    img2 = img.copy()
    sc_y = 0.4 * np.random.rand() + 1.0
    img2 = cv2.resize(img, None, fx=1, fy=sc_y, interpolation=cv2.INTER_CUBIC)

    dy = int((img2.shape[1] - img.shape[0]) / 2)
    end = img.shape[1] - dy
    img2 = img2[dy:end, :, :]
    assert img2.shape[0] == 32
    #     print(img2.shape,dy,end)
    return img2


# Compute linear image transformation ing*s+m
def lin_img(img, s=1.0, m=0.0):
    img2 = cv2.multiply(img, np.array([s]))
    return cv2.add(img2, np.array([m]))


# Change image contrast; s>1 - increase
def contr_img(img, s=1.0):
    m = 127.0 * (1.0 - s)
    return lin_img(img, s, m)


def augment_img(img):
    img = img.copy()
    img = contr_img(img, 1.8*np.random.rand() + 0.2)
    img = random_rotate_img(img)
    img = random_scale_img(img)

    return transform_img(img)

training_file = 'datasets/train.p'
testing_file = 'datasets/test.p'

train,test = load_data(training_file,testing_file)
X_train, y_train = train['features'], train['labels']
X_test, y_test = test['features'], test['labels']
X_train,X_valid,y_train,y_valid = train_test_split(X_train,y_train,test_size=0.2,random_state=42)
n_train = X_train.shape[0]
n_test = X_test.shape[0]
n_valid = X_valid.shape[0]
img_shape = X_train[0].shape
classes = np.unique(y_train).shape[0]
X_train,y_train = shuffle(X_train,y_train)
X_valid,y_valid = shuffle(X_valid,y_valid)
X_test,y_test = shuffle(X_test,y_test)
#to know sizes of data and images dimensions
print(n_train,n_valid,n_test,img_shape,classes)
#X_train_norm,X_valid,y_train,y_dev = train_test_split(X_trained_preprocessed,y_train,test_size=0.2,random_state=0)
"""
X_train_transf = list()
y_train_transf = list()
X_test_transf = list()
X_valid_transf = list()
for ii in range(X_train.shape[0]):
    img = X_train[ii]
    label = y_train[ii]

    imgout = transform_img(img)
    imgout.shape = (1,) + imgout.shape + (1,)
    X_train_transf.append(imgout)
    y_train_transf.append(label)
    for j in range(10):
        imgout = augment_img(img)
        imgout.shape = (1,) + imgout.shape + (1,)
        X_train_transf.append(imgout)
        y_train_transf.append(label)

for ii in range(X_valid.shape[0]):
    img = X_valid[ii]
    img = transform_img(img)
    img.shape = (1,) + img.shape + (1,)
    X_valid_transf.append(img)

for ii in range(X_test.shape[0]):
    img = X_test[ii]
    img = transform_img(img)
    img.shape = (1,) + img.shape + (1,)
    X_test_transf.append(img)

X_test_transf = np.concatenate(X_test_transf, axis=0)
X_train_transf = np.concatenate(X_train_transf, axis=0)
X_valid_transf = np.concatenate(X_valid_transf, axis=0)
y_train_transf = np.array(y_train_transf)

valid = dict()

class_labels = dict()
"""
"""
#to know classes names
with open('signnames.csv',mode='r') as csvfile:
    spamreader = csv.reader(csvfile,delimiter = ',')
    header_skipped = False
    for row in spamreader:
        if not header_skipped:
            header_skipped = True
            continue
        class_labels[int(row[0])] = row[1]
    for key,val in class_labels.items():
        print(key,val)
"""


#X_trained_preprocessed = preprocess_features(X_train)
#X_test_preprocessed = preprocess_features(X_test)

#split training into training and validation



def LeNet(x):
    mu = 0
    sigma = 0.1

    #Layer 1 convolutional Input = 32x32x3. Output = 28x28x6
    conv1_W = tf.Variable(tf.truncated_normal(shape=(5,5,1,6),mean=mu,stddev=sigma))
    conv1_b = tf.Variable(tf.zeros(6))
    conv1 = tf.nn.conv2d(x,conv1_W,strides=[1,1,1,1],padding='VALID') + conv1_b

    #Activation
    conv1 = tf.nn.relu(conv1)

    #pooling Input = 28x28x6. Output = 14x14x6
    conv1 = tf.nn.max_pool(conv1,ksize=[1,2,2,1],strides=[1,2,2,1],padding='VALID')

    #Layer 2 convolutional Output = 10x10x6
    conv2_W = tf.Variable(tf.truncated_normal(shape=(5,5,6,16),mean=mu,stddev=sigma))
    conv2_b = tf.Variable(tf.zeros(16))
    conv2 = tf.nn.conv2d(conv1,conv2_W,strides=[1,1,1,1],padding='VALID') + conv2_b

    #Activation
    conv2 = tf.nn.relu(conv2)

    #pooling input = 10x10x6 output = 5x5x16
    conv2 = tf.nn.max_pool(conv2,ksize=[1,2,2,1],strides=[1,2,2,1],padding='VALID')
    #flatten
    fc0 = flatten(conv2)

    #layer 3 fully connected input 400 output 120
    fc1_W = tf.Variable(tf.truncated_normal(shape=(400,120),mean=mu,stddev=sigma))
    fc1_b = tf.Variable(tf.zeros(120))
    fc1 = tf.matmul(fc0,fc1_W) + fc1_b
    #Activation
    fc1 = tf.nn.relu(fc1)
    dr1 = tf.nn.dropout(fc1,keep_prob)

    #layer 4 fully connected input 120 output 84
    fc2_W = tf.Variable(tf.truncated_normal(shape=(120,84),mean=mu,stddev=sigma))
    fc2_b = tf.Variable(tf.zeros(84))
    fc2 = tf.matmul(dr1,fc2_W) + fc2_b
    #Activation
    fc2 = tf.nn.relu(fc2)

    dr2 = tf.nn.dropout(fc2,keep_prob)

    #layer 5 fc input 43 output 10
    fc3_W = tf.Variable(tf.truncated_normal(shape=(84, 43), mean=mu, stddev=sigma))
    fc3_b = tf.Variable(tf.zeros(43))
    logits = tf.matmul(dr2, fc3_W) + fc3_b

    return logits


mu = 0
sigma = 0.1


def conv2d(x, W, b, strides=1, padding='VALID'):
    x = tf.nn.conv2d(x, W, strides=[1, strides, strides, 1], padding=padding)
    x = tf.nn.bias_add(x, b)
    return tf.nn.relu(x)


def maxpool2d(x, k=2):
    return tf.nn.max_pool(x, ksize=[1, k, k, 1], strides=[1, k, k, 1], padding='VALID')


def inception2d(x, in_channels, filter_count):
    # bias dimension = 3*filter_count and then the extra in_channels for the avg pooling
    bias = tf.Variable(tf.truncated_normal([3 * filter_count + in_channels], mu, sigma))

    # 1x1
    one_filter = tf.Variable(tf.truncated_normal([1, 1, in_channels, filter_count], mu, sigma))
    one_by_one = tf.nn.conv2d(x, one_filter, strides=[1, 1, 1, 1], padding='SAME')

    # 3x3
    three_filter = tf.Variable(tf.truncated_normal([3, 3, in_channels, filter_count], mu, sigma))
    three_by_three = tf.nn.conv2d(x, three_filter, strides=[1, 1, 1, 1], padding='SAME')

    # 5x5
    five_filter = tf.Variable(tf.truncated_normal([5, 5, in_channels, filter_count], mu, sigma))
    five_by_five = tf.nn.conv2d(x, five_filter, strides=[1, 1, 1, 1], padding='SAME')

    # avg pooling
    pooling = tf.nn.avg_pool(x, ksize=[1, 3, 3, 1], strides=[1, 1, 1, 1], padding='SAME')

    x = tf.concat([one_by_one, three_by_three, five_by_five, pooling], axis=3)  # Concat in the 4th dim to stack
    x = tf.nn.bias_add(x, bias)
    return tf.nn.relu(x)

n_channels = 1
n_classes = 43
"""
weights = {
    'wc1': tf.Variable(tf.truncated_normal([5, 5, n_channels, 6 * n_channels], mu, sigma)),
    'wc2': tf.Variable(tf.truncated_normal([5, 5, 6 * n_channels, 16 * n_channels], mu, sigma)),
    'wc3': tf.Variable(tf.truncated_normal([4, 4, 16 * n_channels, 32 * n_channels], mu, sigma)),
    'wf1': tf.Variable(tf.truncated_normal([4 * 4 * 32 * n_channels, 120 * n_channels], mu, sigma)),
    'wf2': tf.Variable(tf.truncated_normal([120 * n_channels, 84 * n_channels], mu, sigma)),
    'out': tf.Variable(tf.truncated_normal([84 * n_channels, n_classes], mu, sigma)),
}
biases = {
    'bc1': tf.Variable(tf.truncated_normal([6 * n_channels], mu, sigma)),
    'bc2': tf.Variable(tf.truncated_normal([16 * n_channels], mu, sigma)),
    'bc3': tf.Variable(tf.truncated_normal([32 * n_channels], mu, sigma)),
    'bf1': tf.Variable(tf.truncated_normal([120 * n_channels], mu, sigma)),
    'bf2': tf.Variable(tf.truncated_normal([84 * n_channels], mu, sigma)),
    'out': tf.Variable(tf.truncated_normal([n_classes], mu, sigma)),
}


def myLeNet(x):
    # Convolution with Activation 32x32x(chan) -> 28x28x(6*chan)
    conv1 = conv2d(x, weights['wc1'], biases['bc1'])

    # Convolution with Activation 28x28x(6*chan) -> 24x24x(16*chan)
    conv2 = conv2d(conv1, weights['wc2'], biases['bc2'])
    # Pooling 24x24x(16*chan) -> 12x12x(16*chan)
    conv2 = maxpool2d(conv2)

    # Convolution with Activation 12x12x(16*chan) -> 8x8x(32*chan)
    conv3 = conv2d(conv2, weights['wc3'], biases['bc3'])
    # Pooling 8x8x(32*chan) -> 4x4x(32*chan)
    conv3 = maxpool2d(conv3)

    # Flatten
    flat = flatten(conv3)

    # Fully-Connected with Activation
    fc1 = tf.add(tf.matmul(flat, weights['wf1']), biases['bf1'])
    fc1 = tf.nn.relu(fc1)

    # Fully-Connected with Activation
    fc2 = tf.add(tf.matmul(fc1, weights['wf2']), biases['bf2'])
    fc2 = tf.nn.relu(fc2)

    # Fully-Connected
    logits = tf.add(tf.matmul(fc2, weights['out']), biases['out'])

    return logits
"""
def LeNetPlusPlus(x):

    conv1_w = tf.Variable(tf.truncated_normal(shape=(3, 3, 3, 64), stddev=0.01))
    conv1_b = tf.Variable(tf.zeros(64))
    conv1 = tf.nn.conv2d(x, conv1_w, strides=[1, 1, 1, 1], padding='SAME') + conv1_b
    conv1 = tf.nn.relu(conv1)
    conv1 = tf.nn.max_pool(conv1, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='VALID')

    conv2_w = tf.Variable(tf.truncated_normal(shape=(3, 3, 64, 64), stddev=0.01))
    conv2_b = tf.Variable(tf.zeros(64))
    conv2 = tf.nn.conv2d(conv1, conv2_w, strides=[1, 1, 1, 1], padding='SAME') + conv2_b
    conv2 = tf.nn.relu(conv2)
    conv2 = tf.nn.max_pool(conv2, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='VALID')

    conv3_w = tf.Variable(tf.truncated_normal(shape=(3, 3, 64, 128), stddev=0.01))
    conv3_b = tf.Variable(tf.zeros(128))
    conv3 = tf.nn.conv2d(conv2, conv3_w, strides=[1, 1, 1, 1], padding='SAME') + conv3_b
    conv3 = tf.nn.relu(conv3)
    conv3 = tf.nn.max_pool(conv3, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='VALID')

    fc1 = flatten(conv3)
    fc1_shape = (fc1.get_shape().as_list()[-1], 1024)
    fc1_w = tf.Variable(tf.truncated_normal(shape=(fc1_shape), stddev=0.01))
    fc1_b = tf.Variable(tf.zeros(1024))
    fc1 = tf.matmul(fc1, fc1_w) + fc1_b
    fc1 = tf.nn.relu(fc1)
    fc1 = tf.nn.dropout(fc1, keep_prob)

    fc2_w = tf.Variable(tf.truncated_normal(shape=(1024, 1024), stddev=0.01))
    fc2_b = tf.Variable(tf.zeros(1024))
    fc2 = tf.matmul(fc1, fc2_w) + fc2_b
    fc2 = tf.nn.relu(fc2)
    fc2 = tf.nn.dropout(fc2, keep_prob)

    fc3_w = tf.Variable(tf.truncated_normal(shape=(1024, 43), stddev=0.01))
    fc3_b = tf.Variable(tf.zeros(43))
    return (tf.matmul(fc2, fc3_w) + fc3_b), [conv1_w, conv2_w, fc1_w, fc2_w, fc3_w]

def preprocess(X):
    t = []
    for i in range(0, len(X)):
        gray = cv2.cvtColor(X[i], cv2.COLOR_RGB2GRAY)
        blur = cv2.GaussianBlur(gray, (5,5), 20.0)
        image = cv2.addWeighted(gray, 2, blur, -1, 0)
        image = cv2.equalizeHist(image)
        image = equalize_hist(image)
        t.append(image)
    X = np.reshape(t, (-1, 64, 64, 1))
    print("Image Shape: {}".format(X.shape))
    return X

def LeNet2(x):
    # Arguments used for tf.truncated_normal, randomly defines variables for the weights and biases for each layer
    mu = 0
    sigma = 0.1
    ft_sz = 3
    weights = []

    # TODO: Layer 1: Convolutional. Input = 64x64x1. Output = 64x64x8.
    conv1_W = tf.Variable(tf.truncated_normal(shape=(ft_sz,ft_sz,1,8), mean = mu, stddev = sigma))
    conv1_b = tf.Variable(tf.zeros(8))
    conv1   = tf.nn.conv2d(x, conv1_W, strides = [1, 1, 1, 1], padding = 'SAME') + conv1_b
    regularizers = tf.nn.l2_loss(conv1_W)
    # TODO: Activation.
    conv1 = tf.nn.relu(conv1)
    weights.append(conv1_W)

    conv1_2_W = tf.Variable(tf.truncated_normal(shape=(3,3,8,8), mean = mu, stddev = sigma))
    conv1_2_b = tf.Variable(tf.zeros(8))
    conv1   = tf.nn.conv2d(conv1, conv1_2_W, strides = [1, 1, 1, 1], padding = 'SAME') + conv1_2_b
    regularizers = tf.nn.l2_loss(conv1_2_W)
    # TODO: Activation.
    conv1 = tf.nn.relu(conv1)
    weights.append(conv1_2_W)

    # TODO: Pooling. Input = 64x64x8. Output = 32x32x8.
    conv1 = tf.nn.max_pool(conv1, ksize = [1, 2, 2, 1], strides = [1, 2, 2, 1], padding = 'SAME')

    # TODO: Layer 2: Convolutional. Output = 32x32x16.
    conv2_W = tf.Variable(tf.truncated_normal(shape=(ft_sz, ft_sz, 8, 16), mean = mu, stddev = sigma))
    conv2_b = tf.Variable(tf.zeros(16))
    conv2   = tf.nn.conv2d(conv1, conv2_W, strides = [1, 1, 1, 1], padding = 'SAME') + conv2_b
    regularizers += tf.nn.l2_loss(conv2_W)
    # TODO: Activation.
    conv2 = tf.nn.relu(conv2)
    weights.append(conv2_W)

    conv2_2_W = tf.Variable(tf.truncated_normal(shape=(ft_sz,ft_sz,16,16), mean = mu, stddev = sigma))
    conv2_2_b = tf.Variable(tf.zeros(16))
    conv2   = tf.nn.conv2d(conv2, conv2_2_W, strides = [1, 1, 1, 1], padding = 'SAME') + conv2_2_b
    regularizers = tf.nn.l2_loss(conv2_2_W)
    # TODO: Activation.
    conv2 = tf.nn.relu(conv2)
    weights.append(conv2_2_W)

    # TODO: Pooling. Input = 32x32x16. Output = 16x16x16.
    conv2 = tf.nn.max_pool(conv2, ksize = [1, 2, 2, 1], strides = [1, 2, 2, 1], padding = 'SAME')

    # TODO: Layer 2: Convolutional. Output = 16x16x32.
    conv3_W = tf.Variable(tf.truncated_normal(shape=(ft_sz, ft_sz, 16, 32), mean = mu, stddev = sigma))
    conv3_b = tf.Variable(tf.zeros(32))
    conv3   = tf.nn.conv2d(conv2, conv3_W, strides = [1, 1, 1, 1], padding = 'SAME') + conv3_b
    regularizers += tf.nn.l2_loss(conv3_W)
    # TODO: Activation.
    conv3 = tf.nn.relu(conv3)
    weights.append(conv3_W)

    # TODO: Layer 1: Convolutional. Input = 16x16x16. Output = 16x16x32.
    conv3_2_W = tf.Variable(tf.truncated_normal(shape=(ft_sz,ft_sz,32,32), mean = mu, stddev = sigma))
    conv3_2_b = tf.Variable(tf.zeros(32))
    conv3   = tf.nn.conv2d(conv3, conv3_2_W, strides = [1, 1, 1, 1], padding = 'SAME') + conv3_2_b
    regularizers = tf.nn.l2_loss(conv3_2_W)
    # TODO: Activation.
    conv3 = tf.nn.relu(conv3)
    weights.append(conv3_2_W)

    # TODO: Pooling. Input = 16x16x32. Output = 8x8x32.
    conv3 = tf.nn.max_pool(conv3, ksize = [1, 2, 2, 1], strides = [1, 2, 2, 1], padding = 'SAME')

    # TODO: Layer 2: Convolutional. Output = 8x8x64.
    conv4_W = tf.Variable(tf.truncated_normal(shape=(ft_sz, ft_sz, 32, 64), mean = mu, stddev = sigma))
    conv4_b = tf.Variable(tf.zeros(64))
    conv4   = tf.nn.conv2d(conv3, conv4_W, strides = [1, 1, 1, 1], padding = 'SAME') + conv4_b
    regularizers += tf.nn.l2_loss(conv4_W)
    # TODO: Activation.
    conv4 = tf.nn.relu(conv4)
    weights.append(conv4_W)

    # TODO: Layer 1: Convolutional. Input = 8x8x32. Output = 8x8x64.
    conv4_2_W = tf.Variable(tf.truncated_normal(shape=(ft_sz,ft_sz,64,64), mean = mu, stddev = sigma))
    conv4_2_b = tf.Variable(tf.zeros(64))
    conv4   = tf.nn.conv2d(conv4, conv4_2_W, strides = [1, 1, 1, 1], padding = 'SAME') + conv4_2_b
    regularizers = tf.nn.l2_loss(conv4_2_W)
    # TODO: Activation.
    conv4 = tf.nn.relu(conv4)
    weights.append(conv4_2_W)

    # TODO: Pooling. Input = 8x8x64. Output = 4x4x64.
    conv4 = tf.nn.max_pool(conv4, ksize = [1, 2, 2, 1], strides = [1, 2, 2, 1], padding = 'SAME')

    # TODO: Layer 2: Convolutional. Output = 4x4x128.
    conv5_W = tf.Variable(tf.truncated_normal(shape=(ft_sz, ft_sz, 64, 128), mean = mu, stddev = sigma))
    conv5_b = tf.Variable(tf.zeros(128))
    conv5   = tf.nn.conv2d(conv4, conv5_W, strides = [1, 1, 1, 1], padding = 'SAME') + conv5_b
    regularizers += tf.nn.l2_loss(conv5_W)
    # TODO: Activation.
    conv5 = tf.nn.relu(conv5)
    weights.append(conv5_W)

    # TODO: Layer 1: Convolutional. Input = 4x4x64. Output = 4x4x128.
    conv5_2_W = tf.Variable(tf.truncated_normal(shape=(ft_sz,ft_sz,128,128), mean = mu, stddev = sigma))
    conv5_2_b = tf.Variable(tf.zeros(128))
    conv5   = tf.nn.conv2d(conv5, conv5_2_W, strides = [1, 1, 1, 1], padding = 'SAME') + conv5_2_b
    regularizers = tf.nn.l2_loss(conv5_2_W)
    # TODO: Activation.
    conv5 = tf.nn.relu(conv5)
    weights.append(conv5_2_W)

    # TODO: Pooling. Input = 4x4x128. Output = 2x2x128.
    conv5 = tf.nn.max_pool(conv5, ksize = [1, 2, 2, 1], strides = [1, 2, 2, 1], padding = 'SAME')

    # TODO: Flatten. Input = 2x2x128. Output = 512.
    fc0   = flatten(conv5)

    # TODO: Layer 3: Fully Connected. Input = 2048. Output = 120.
    fc1_W  = tf.Variable(tf.truncated_normal(shape=(512, 43), mean = mu, stddev = sigma))
    fc1_b  = tf.Variable(tf.zeros(43))
    regularizers += tf.nn.l2_loss(fc1_W)
    logits = tf.matmul(fc0, fc1_W) + fc1_b

    return [logits, regularizers, weights, fc0]



x = tf.placeholder(tf.float32, (None, 64, 64, 1), name="input_data")
y = tf.placeholder(tf.int32, (None))
keep_prob = tf.placeholder(tf.float32)
one_hot_y = tf.one_hot(y,43)


logits, regularizers, weights, features = LeNet2(x)
cross_entropy = tf.nn.softmax_cross_entropy_with_logits(logits=logits, labels=one_hot_y)
loss_operation = tf.reduce_mean(cross_entropy)
optimizer = tf.train.AdamOptimizer(learning_rate=0.0001)
training_operation = optimizer.minimize(loss_operation)
predict_operation = tf.argmax(logits,1)
predict_prob_operation = tf.nn.softmax(logits=logits)
correct_prediction = tf.equal(tf.argmax(logits,1),tf.argmax(one_hot_y,1))
accuracy_operation = tf.reduce_mean(tf.cast(correct_prediction,tf.float32))

def evaluate(X_data,y_data):
    num_examples = len(X_data)
    total_accuracy = 0
    sess = tf.get_default_session()
    for offset in range(0,num_examples,BATCH_SIZE):
        batch_x,batch_y = X_data[offset:offset+BATCH_SIZE], y_data[offset:offset+BATCH_SIZE]
        accuracy = sess.run(accuracy_operation,feed_dict ={x:batch_x,y:batch_y,keep_prob:1.0})
        total_accuracy += (accuracy * len(batch_x))
    return total_accuracy / num_examples

def predict(X_data):
    num_examples = len(X_data)
    sess = tf.get_default_session()
    predicted_probs = list()
    for offset in range(0,num_examples,BATCH_SIZE):
        batch_x = X_data[offset:offset+BATCH_SIZE]
        predicted_probs.extend(sess.run(predict_prob_operation,feed_dict={x:batch_x,keep_prob:1.0}))
    return predicted_probs

logger = logging.getLogger()
"""
X_train = X_train_transf
X_valid = X_valid_transf
X_test = X_test_transf
y_train = y_train_transf
"""
X_train = preprocess(X_train)
X_valid = preprocess(X_valid)
X_test = preprocess(X_test)
X_train, y_train = shuffle(X_train, y_train)

def setup_file_logger(log_file):
    hdlr = logging.FileHandler(log_file)
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(logging.INFO)

def log(message):
    #outputs to Jupyter console
    print('{} {}'.format(datetime.datetime.now(), message))
    #outputs to file
    logger.info(message)

setup_file_logger('training.log')
EPOCHS = 150
BATCH_SIZE = 128
dropout = 0.2

errors = list()

saver = tf.train.Saver()
start = time()

with tf.Session() as sess:

    sess.run(tf.global_variables_initializer())
    num_examples = len(X_train)
    log("Training... dropout = {} , batch_size = {} , learning rate = {}".format(dropout, BATCH_SIZE, 0.0001))
    print()
    for i in range(EPOCHS):
        try:
            X_train,y_train = shuffle(X_train,y_train)
            for offset in range(0,num_examples,BATCH_SIZE):
                end = offset + BATCH_SIZE
                batch_x,batch_y = X_train[offset:end],y_train[offset:end]
                sess.run(training_operation,feed_dict={x: batch_x, y: batch_y, keep_prob: 1 - dropout})

            validation_accuracy = evaluate(X_valid,y_valid)
            training_accuracy = evaluate(X_train,y_train)

            errors.append((training_accuracy,validation_accuracy))
            log("EPOCH %d - %d sec ..." % (i + 1, time() - start))
            log("Training accuracy = {:.3f} Validation accuracy = {:.3f}".format(training_accuracy,validation_accuracy))
            print()
            if i > 5 and i % 3 == 0:
                saver.save(sess, './models/lenet')
                print("Model saved %d sec" % (time() - start))
        except KeyboardInterrupt:
            print('Accuracy Model On Test Images: {}'.format(evaluate(X_test, y_test)))
            break
    saver.save(sess,'.models/lenet')

#this part gives error on line 327

#plt.figure(figsize=(8,6))
#plt.title('Learning Curve')
#plt.plot([1 - el[0] for el in errors])
#plt.plot([1 - el[1] for el in errors])
#plt.ylim([-.01,0.35])
#plt.legend(['Training Error','Validation Error'])
#plt.tight_layout()
#plt.savefig('plots/learning_curve.png')
#plt.ylabel('Error')
#plt.xlabel('Epoch')
def test():
    with tf.Session() as sess:
        saver = tf.train.Saver()
        saver.restore(sess, tf.train.latest_checkpoint('./models'))
        print('Accuracy Model On Training Images: {:.2f}'.format(evaluate(X_train,y_train)))
        print('Accuracy Model On Validation Images: {:.2f}'.format(evaluate(X_valid,y_valid)))
        print('Accuracy Model On Test Images: {:.2f}'.format(evaluate(X_test,y_test)))

def run_images():
    images_raw = list()
    labels_raw = list()
    for line in open('images_signs/data.txt','r'):
        fname, label = line.strip().split(' ')
        label = int(label)
        fname = 'images_signs/'+fname
        img = cv2.imread(fname)
        img = resize(img,(32,32),order=3)
        img = gaussian(img,.6,multichannel=True)*255
        img = transform_img(img.astype(np.uint8))
        img.shape = (1,) + img.shape + (1,)
        images_raw.append(img)
        labels_raw.append(label)
    images = np.concatenate(images_raw,axis=0)
    #print("concatenated check")

    with tf.Session() as sess:
        saver = tf.train.Saver()
        saver.restore(sess, tf.train.latest_checkpoint('./models'))

        predicted_probs = np.vstack(predict(images))

        print('Accuracy Model On Internet Images: {}'.format(evaluate(images, labels_raw)))
        print(predicted_probs)
        for true_label, row in zip(labels_raw, predicted_probs):
            top5k = np.argsort(row)[::-1][:5]
            top5p = np.sort(row)[::-1][:5]
            print('Top 5 Labels for image \'{}\':'.format(get_name_from_label(true_label)))
            for k, p in zip(top5k, top5p):
                print(' - \'{}\' with prob = {:.2f} '.format(get_name_from_label(k), p))

test()
run_images()
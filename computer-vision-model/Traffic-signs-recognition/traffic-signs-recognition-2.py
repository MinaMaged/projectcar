import cv2
import numpy as np
import csv
import pickle
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
from tensorflow.contrib.layers import flatten
from sklearn.model_selection import train_test_split
from keras.preprocessing.image import ImageDataGenerator
from sklearn.utils import shuffle
from time import time
import logging, datetime
from skimage.transform import resize
from skimage.filters import gaussian
import pandas as pd
from skimage.exposure import equalize_hist
from tqdm import tqdm
from PIL import Image
from PIL import ImageEnhance
import random

training_file = "datasets/train.p"
testing_file = "datasets/test.p"

with open(training_file, mode='rb') as f:
    train = pickle.load(f)
with open(testing_file, mode='rb') as f:
    test = pickle.load(f)

X_train, y_train = train['features'], train['labels']
X_test, y_test = test['features'], test['labels']
X_train,X_valid,y_train,y_valid = train_test_split(X_train,y_train,test_size=0.2,random_state=42)
X_train, y_train = shuffle(X_train, y_train)

n_train = len(X_train)
n_test = len(X_test)
image_shape = X_train[0].shape
n_classes = len(set(y_train))

print("Number of training examples =", n_train)
print("Number of testing examples =", n_test)
print("Image data shape =", image_shape)
print("Number of classes =", n_classes)



print_accuracy_mod_frequency = 5

def get_name_from_label(label):
    # Helper, transofrm a numeric label into the corresponding strring
    return signnames.loc[label].SignName


def show_dataset_summary(pickle_dict):
    X, y = pickle_dict['features'], pickle_dict['labels']
    n_classes = np.max(y_train) - np.min(y_train) + 1
    n_data_of_classes = np.zeros((n_classes,))
    for i in range(n_classes):
        n_data_of_classes[i] = len(y[y == i])

    classes_num = [i for i in range(n_classes)]

def preprocess(X):
    t = []
    for i in range(0, len(X)):
        gray = cv2.cvtColor(X[i], cv2.COLOR_RGB2GRAY)
        blur = cv2.GaussianBlur(gray, (5,5), 20.0)
        image = cv2.addWeighted(gray, 2, blur, -1, 0)
        image = cv2.equalizeHist(image)
        image = equalize_hist(image)
        t.append(image)
    X = np.reshape(t, (-1, 32, 32, 1))
    print("Image Shape: {}".format(X.shape))
    return X

generate_distorted_images = False
if generate_distorted_images == True:
    # initialize augmented (X,y)
    X_train_augmented = X_train
    y_train_augmented = y_train

    for i in range(X_train.shape[0]):
        # If you have less than 3000 data, you increase the number of data by random cropping.

            # load training images
        X_train_PIL = Image.fromarray(X_train[i, :, :, :])

            # set width(=height) and start points for random cropping
        rw = np.floor(random.random() * 12 + 18)  # random width (18~30)
        rw = int(rw)
        rs = np.floor((32 - rw) * random.random())  # random crop start point
        rs = int(rs)

            # randomly crop and reshape to (32,32,3)
        randomly_cropped_image = X_train_PIL.crop((rs, rs, rs + rw, rs + rw))
        distorted_image = randomly_cropped_image.resize((32, 32), Image.ANTIALIAS)

            # randomly adjust brightness
        enhancer = ImageEnhance.Brightness(distorted_image)
        distorted_image_ = enhancer.enhance(random.random())

            # convert image to uint8 array
        distorted_image_ = np.array(distorted_image_, dtype=np.uint8)

            # append distorted image on X_train
        distorted_image_ = distorted_image_[np.newaxis, :]  # (expand dimension from (32,32,3) to (1,32,32,3))
        X_train_augmented = np.append(X_train_augmented, distorted_image_, axis=0)
        y_train_augmented = np.append(y_train_augmented, [y_train[i]], axis=0)


    print("augmented training images are generated.")
    print("%d -> %d" % (X_train.shape[0], X_train_augmented.shape[0]))
    if generate_distorted_images == True:
        # make dictionary
        train_augmented = {'features': X_train_augmented, 'labels': y_train_augmented}
    # save augmented training dataset
        adata_name = "train_aug.p"
        with open(adata_name, "wb") as f:
            pickle.dump(train_augmented, f)
with open("train_aug.p", mode="rb") as f:
    train = pickle.load(f)

X_train, y_train = train['features'], train['labels']

def normalize_images(X_):
    return X_ / 255 - 0.5
"""
print("Validation data")
X_valid = preprocess(X_valid)
print("Test data")
X_test = preprocess(X_test)
X_train, y_train = shuffle(X_train, y_train)
"""
X_train_ = normalize_images(X_train)
print('training set is normalized')

X_valid_ = normalize_images(X_valid)
print('Validation set is normalized')

X_test_ = normalize_images(X_test)
print('Test set is normalized')

EPOCHS = 150
BATCH_SIZE = 128
rate = 0.001
dropout = 0.2
errors = list()

def LeNet_he(x):
    # Arguments used for tf.truncated_normal, randomly defines variables for the weights and biases for each layer
    mu = 0
    sigma = 0.1

    # Layer 1: Convolutional. Input = 32x32x3. Output = 28x28x32.
    conv1_w = tf.Variable(tf.truncated_normal(shape=(5,5,3,32), mean=mu, stddev=np.sqrt(2/(5*5*3))))
    conv1_b = tf.Variable(tf.zeros(32))
    conv1 = tf.nn.conv2d(x, conv1_w, strides=[1,1,1,1], padding='VALID') + conv1_b

    # batch normalization
    mean_, var_ = tf.nn.moments(conv1, [0,1,2])
    conv1 = tf.nn.batch_normalization(conv1, mean_, var_, 0, 1, 0.0001)


    # Activation.
    conv1 = tf.nn.relu(conv1)

    # Pooling. Input = 28x28x32. Output = 14x14x32.
    conv1 = tf.nn.max_pool(conv1, ksize=[1,2,2,1],strides=[1,2,2,1], padding='VALID')

    # Layer 2: Convolutional. Output = 10x10x64.
    conv2_w = tf.Variable(tf.truncated_normal(shape=(5,5,32,64), mean=mu, stddev=np.sqrt(2/(5*5*32))))
    conv2_b = tf.Variable(tf.zeros(64))
    conv2 = tf.nn.conv2d(conv1, conv2_w, strides=[1,1,1,1], padding='VALID') + conv2_b

    # batch normalization
    mean_, var_ = tf.nn.moments(conv2, [0,1,2])
    conv2 = tf.nn.batch_normalization(conv2, mean_, var_, 0, 1, 0.0001)


    # Activation.
    conv2 = tf.nn.relu(conv2)

    # Pooling. Input = 10x10x64. Output = 5x5x64.
    conv2 = tf.nn.max_pool(conv2, ksize=[1,2,2,1], strides=[1,2,2,1], padding='VALID')

    # Flatten. Input = 5x5x64. Output = 1600.
    fc0 = flatten(conv2)

    # Layer 3: Fully Connected. Input = 1600. Output = 120.
    fc1_w = tf.Variable(tf.truncated_normal(shape=(1600,120), mean=mu, stddev=np.sqrt(2/(1600))))
    fc1_b = tf.Variable(tf.zeros(120))
    fc1 = tf.matmul(fc0, fc1_w) + fc1_b

    # batch normalization
    mean_, var_ = tf.nn.moments(fc1, axes=[0])
    fc1 = tf.nn.batch_normalization(fc1, mean_, var_, 0, 1, 0.0001)

    # Activation.
    fc1 = tf.nn.relu(fc1)

    # Layer 4: Fully Connected. Input = 120. Output = 84.
    fc2_w = tf.Variable(tf.truncated_normal(shape=(120,84), mean=mu, stddev=np.sqrt(2/120)))
    fc2_b = tf.Variable(tf.zeros(84))
    fc2 = tf.matmul(fc1, fc2_w) + fc2_b

    # batch normalization
    mean_, var_ = tf.nn.moments(fc2, axes=[0])
    fc2 = tf.nn.batch_normalization(fc2, mean_, var_, 0, 1, 0.0001)

    # Activation.
    fc2 = tf.nn.relu(fc2)

    # Layer 5: Fully Connected. Input = 84. Output = 43.
    fc3_w = tf.Variable(tf.truncated_normal(shape=(84,43), mean=mu, stddev=np.sqrt(2/84)))
    fc3_b = tf.Variable(tf.zeros(43))
    logits = tf.matmul(fc2, fc3_w) + fc3_b

    return logits

print('LeNet w/ He initialziation is ready')

x = tf.placeholder(tf.float64, (None, 32, 32, 3))
x = tf.cast(x, tf.float32)
y = tf.placeholder(tf.uint8, (None))
one_hot_y = tf.one_hot(y, n_classes)
keep_prob = tf.placeholder(tf.float32)

logits = LeNet_he(x)
cross_entropy = tf.nn.softmax_cross_entropy_with_logits(logits=logits, labels=one_hot_y)

loss_operation = tf.reduce_mean(cross_entropy)

optimizer = tf.train.AdamOptimizer(learning_rate=rate)
training_operation = optimizer.minimize(loss_operation)

correct_prediction = tf.equal(tf.argmax(logits, 1), tf.argmax(one_hot_y, 1))
accuracy_operation = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

prediction = tf.argmax(logits, 1, name="prediction")
correct_prediction = tf.equal(prediction, tf.argmax(one_hot_y, 1))
accuracy_operation = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
saver = tf.train.Saver()

def evaluate(X_data,y_data):
    num_examples = len(X_data)
    total_accuracy = 0
    sess = tf.get_default_session()
    for offset in range(0,num_examples,BATCH_SIZE):
        batch_x,batch_y = X_data[offset:offset+BATCH_SIZE], y_data[offset:offset+BATCH_SIZE]
        accuracy = sess.run(accuracy_operation,feed_dict ={x:batch_x,y:batch_y,keep_prob:1.0})
        total_accuracy += (accuracy * len(batch_x))
    return total_accuracy / num_examples


logger = logging.getLogger()
def setup_file_logger(log_file):
    hdlr = logging.FileHandler(log_file)
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(logging.INFO)

def log(message):
    #outputs to Jupyter console
    print('{} {}'.format(datetime.datetime.now(), message))
    #outputs to file
    logger.info(message)

setup_file_logger('training.log')

do_train = 0
start = time()
with tf.Session() as sess:
    if do_train==1:
        sess.run(tf.global_variables_initializer())
        num_examples = len(X_train)
        log("Training... dropout = {} , batch_size = {} , learning rate = {}".format(dropout, BATCH_SIZE, rate))
        print()
        for i in range(EPOCHS):
            try:
                X_train,y_train = shuffle(X_train,y_train)
                for offset in range(0,num_examples,BATCH_SIZE):
                    end = offset + BATCH_SIZE
                    batch_x,batch_y = X_train[offset:end],y_train[offset:end]
                    sess.run(training_operation,feed_dict={x: batch_x, y: batch_y})

                validation_accuracy = evaluate(X_valid,y_valid)
                training_accuracy = evaluate(X_train,y_train)

                errors.append((training_accuracy,validation_accuracy))
                log("EPOCH %d - %d sec ..." % (i + 1, time() - start))
                log("Training accuracy = {:.3f} Validation accuracy = {:.3f}".format(training_accuracy,validation_accuracy))
                print()
                if i > 5 and i % 3 == 0:
                    saver.save(sess, './models/lenet')
                    print("Model saved %d sec" % (time() - start))
            except KeyboardInterrupt:
                print('Accuracy Model On Test Images: {}'.format(evaluate(X_test, y_test)))
                break
        saver.save(sess,'.models/lenet')
def test():
    with tf.Session() as sess:
        saver = tf.train.Saver()
        saver.restore(sess, tf.train.latest_checkpoint('./models'))
        print('Accuracy Model On Training Images: {:.2f}'.format(evaluate(X_train,y_train)))
        print('Accuracy Model On Validation Images: {:.2f}'.format(evaluate(X_valid,y_valid)))
        print('Accuracy Model On Test Images: {:.2f}'.format(evaluate(X_test,y_test)))
        log("Accuracy Model On Training Images: {:.2f}".format(evaluate(X_train,y_train)))
        log("Accuracy Model On Validation Images: {:.2f}".format(evaluate(X_valid,y_valid)))
        log("Accuracy Model On Test Images: {:.2f}".format(evaluate(X_test,y_test)))

def run_images():
    images_raw = list()
    labels_raw = list()
    for line in open('images_signs/data.txt','r'):
        fname, label = line.strip().split(' ')
        label = int(label)
        fname = 'images_signs/'+fname
        img = cv2.imread(fname)
        img = resize(img,(32,32),order=3)
        img = gaussian(img,.6,multichannel=True)*255
        img = transform_img(img.astype(np.uint8))
        img.shape = (1,) + img.shape + (1,)
        images_raw.append(img)
        labels_raw.append(label)
    images = np.concatenate(images_raw,axis=0)
    #print("concatenated check")

    with tf.Session() as sess:
        saver = tf.train.Saver()
        saver.restore(sess, tf.train.latest_checkpoint('./models'))

        predicted_probs = np.vstack(predict(images))

        print('Accuracy Model On Internet Images: {}'.format(evaluate(images, labels_raw)))
        print(predicted_probs)
        for true_label, row in zip(labels_raw, predicted_probs):
            top5k = np.argsort(row)[::-1][:5]
            top5p = np.sort(row)[::-1][:5]
            print('Top 5 Labels for image \'{}\':'.format(get_name_from_label(true_label)))
            for k, p in zip(top5k, top5p):
                print(' - \'{}\' with prob = {:.2f} '.format(get_name_from_label(k), p))

test()
run_images()


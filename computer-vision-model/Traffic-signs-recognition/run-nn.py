import cv2
import numpy as np
import csv
import pickle
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
from tensorflow.contrib.layers import flatten
from sklearn.model_selection import train_test_split
from keras.preprocessing.image import ImageDataGenerator
from sklearn.utils import shuffle
from time import time
import logging, datetime
from skimage.transform import resize
from skimage.filters import gaussian

def sharpen_img(img):
    gb = cv2.GaussianBlur(img, (5, 5), 20.0)
    return cv2.addWeighted(img, 2, gb, -1, 0)


def transform_img(img_in):
    img_in = img_in.copy()
    img_out = sharpen_img(img_in)
    img_out = cv2.cvtColor(img_in, cv2.COLOR_RGB2YUV)

    img_out[:, :, 0] = cv2.equalizeHist(img_out[:, :, 0])
    #     img_out[:,:,1] = cv2.equalizeHist(img_out[:,:,1])
    #     img_out[:,:,2] = cv2.equalizeHist(img_out[:,:,2])

    return img_out[:, :, 0]


def random_rotate_img(img):
    c_x, c_y = int(img.shape[0] / 2), int(img.shape[1] / 2)
    ang = 30.0 * np.random.rand() - 15
    Mat = cv2.getRotationMatrix2D((c_x, c_y), ang, 1.0)
    return cv2.warpAffine(img, Mat, img.shape[:2])


def random_scale_img(img):
    img2 = img.copy()
    sc_y = 0.4 * np.random.rand() + 1.0
    img2 = cv2.resize(img, None, fx=1, fy=sc_y, interpolation=cv2.INTER_CUBIC)

    dy = int((img2.shape[1] - img.shape[0]) / 2)
    end = img.shape[1] - dy
    img2 = img2[dy:end, :, :]
    assert img2.shape[0] == 32
    #     print(img2.shape,dy,end)
    return img2


# Compute linear image transformation ing*s+m
def lin_img(img, s=1.0, m=0.0):
    img2 = cv2.multiply(img, np.array([s]))
    return cv2.add(img2, np.array([m]))


# Change image contrast; s>1 - increase
def contr_img(img, s=1.0):
    m = 127.0 * (1.0 - s)
    return lin_img(img, s, m)


def augment_img(img):
    img = img.copy()
    img = contr_img(img, 1.8*np.random.rand() + 0.2)
    img = random_rotate_img(img)
    img = random_scale_img(img)

    return transform_img(img)

def run_images():
    images_raw = list()
    labels_raw = list()
    for line in open('images_signs/data.txt','r'):
        fname,label = line.strip().split(' ')
        label = int(label)
        fname = 'images_signs/'+fname
        img = cv2.imread(fname)
        img = resize(img,(32,32),order=3)
        img = gaussian(img,.6,multichannel=True)*255
        img = transform_img(img.astype(np.uint8))
        img.shape = (1,) + img.shape + (1,)
        images_raw.append(img)
        labels_raw.append(label)
    images = np.concatenate(images_raw,axis=0)

    with tf.Session() as sess:
        saver = tf.train.Saver()
        saver.restore(sess, tf.train.latest_checkpoint('./models'))

        predicted_probs = np.vstack(predict(images))

        print('Accuracy Model On Internet Images: {}'.format(evaluate(images, labels_raw)))


run_images()
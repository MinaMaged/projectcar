# -*- coding: utf-8 -*-
import numpy as np
import cv2


def displayImage(image,title):
    cv2.imshow(title,image)
    cv2.waitKey(0)
    #cv2.destroyAllWindows()

def region_of_interest(img, vertices):
    """
    Applies an image mask.
    
    Only keeps the region of the image defined by the polygon
    formed from `vertices`. The rest of the image is set to black.
    """
    #defining a blank mask to start with
    mask = np.zeros_like(img)   
    
    #defining a 3 channel or 1 channel color to fill the mask with depending on the input image
    if len(img.shape) > 2:
        channel_count = img.shape[2]  # i.e. 3 or 4 depending on your image
        ignore_mask_color = (255,) * channel_count
    else:
        ignore_mask_color = 255
        
    #filling pixels inside the polygon defined by "vertices" with the fill color    
    cv2.fillPoly(mask, vertices, ignore_mask_color)
    
    #returning the image only where mask pixels are nonzero
    masked_image = cv2.bitwise_and(img, mask)
    return masked_image

def get_slope(x1,y1,x2,y2):
    return (y2-y1)/(x2-x1)




def hough_lines(img, rho, theta, threshold, min_line_len, max_line_gap):
    """
    `img` should be the output of a Canny transform.
    Returns an image with hough lines drawn.
    """
    lines = cv2.HoughLinesP(img, rho, theta, threshold, np.array([]), min_line_len, max_line_gap)
    line_img = np.zeros((img.shape[0], img.shape[1], 3), dtype=np.uint8)
    draw_lines(line_img, lines) #in case of error remove avg_lines
    print 'entered average lines'
    return line_img #in case of error change back to line_img

def draw_lines(img, lines, color=[0, 200, 0], thickness=2):
    """
    NOTE: this is the function you might want to use as a starting point once you want to
    average/extrapolate the line segments you detect to map out the full
    extent of the lane (going from the result shown in raw-lines-example.mp4
    to that shown in P1_example.mp4).
    Think about things like separating line segments by their
    slope ((y2-y1)/(x2-x1)) to decide which segments are part of the left
    line vs. the right line.  Then, you can average the position of each of
    the lines and extrapolate to the top and bottom of the lane.
    This function draws `lines` with `color` and `thickness`.
    Lines are drawn on the image inplace (mutates the image).
    If you want to make the lines semi-transparent, think about combining
    this function with the weighted_img() function below
    """
    for line in lines:
        for x1,y1,x2,y2 in line:
            cv2.line(img, (x1, y1), (x2, y2), color, thickness)

def weighted_img(img, initial_img, alpha=0.8, rho=1., beta=0.):
    """
    `img` is the output of the hough_lines(), An image with lines drawn on it.
    Should be a blank image (all black) with lines drawn on it.
    `initial_img` should be the image before any processing.
    The result image is computed as follows:
    initial_img * α + img * β + λ
    NOTE: initial_img and img must be the same shape!
    """
    return cv2.addWeighted(initial_img, alpha, img, rho, beta)

def average_slope_intercept(lines):
    left_lines    = [] # (slope, intercept)
    left_weights  = [] # (length,)
    right_lines   = [] # (slope, intercept)
    right_weights = [] # (length,)
    
    for line in lines:
        for x1, y1, x2, y2 in line:
            if x2==x1:
                continue # ignore a vertical line
            slope = (y2-y1)/(x2-x1)
            intercept = y1 - slope*x1
            length = np.sqrt((y2-y1)**2+(x2-x1)**2)
            if slope < 0: # y is reversed in image
                left_lines.append((slope, intercept))
                left_weights.append((length))
            else:
                right_lines.append((slope, intercept))
                right_weights.append((length))
    
    # add more weight to longer lines    
    left_lane  = np.dot(left_weights,  left_lines) /np.sum(left_weights)  if len(left_weights) >0 else None
    right_lane = np.dot(right_weights, right_lines)/np.sum(right_weights) if len(right_weights)>0 else None
    
    return left_lane, right_lane # (slope, intercept), (slope, intercept)



vc = cv2.VideoCapture('project_video.mp4') # 0 return gray color
#img = cv2.imwrite('image.jpg',img) #to save image
#displayImage(img,'Original')
if vc.isOpened():
    rval = True
    print 'true'
else:
    rval = False
    print 'false'
while rval:
    #print rval
    rval , frame = vc.read()
    if rval == False:
        break
    #convert to grayscale
    gray_image = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    
    #displayImage(gray_image,'Gray Image')

    #convert to HSV
    hsv_image = cv2.cvtColor(frame, cv2.COLOR_RGB2HSV)

    #get only white and yellow as yellow
    lower_yellow = np.array([50, 50, 50], dtype = "uint8")
    upper_yellow = np.array([110, 255, 255], dtype="uint8")
    mask_yellow = cv2.inRange(hsv_image,lower_yellow,upper_yellow)
    mask_white = cv2.inRange(gray_image,200,255)
    mask_yw = cv2.bitwise_or(mask_white,mask_yellow)
    mask_yw_image = cv2.bitwise_and(gray_image,mask_yw)
    #displayImage(mask_yw_image,'yellow and white only')

    #gaussian filter
    kernel = 5
    #gaussian_gray = gaussian_blur(mask_yw_image,kernel)
    gaussian_gray = cv2.blur(mask_yw_image,(kernel,kernel))
    #displayImage(gaussian_gray,'blurred')

    #canny edges
    low_threshold = 50
    high_threshold = 150
    canny_edged_image = cv2.Canny(gaussian_gray,low_threshold,high_threshold)
    #displayImage(canny_edged_image,'canny edged image')

    #mask out areas out of lane
    max_width = canny_edged_image.shape[1]
    max_height = canny_edged_image.shape[0]
    width_delta = int(max_width/20)
    vertices = np.array([[(100, max_height), (max_width -100, max_height), (max_width/2 + width_delta, max_height/2 + 50), (max_width/2 - width_delta, max_height/2 + 50)]], np.int32)
    masked_image = region_of_interest(canny_edged_image,vertices)
    #masked_image = select_region(canny_edged_image)
    #displayImage(masked_image,'masked out image')

    #hough transform lines

    lines_image = hough_lines(masked_image, 1, np.pi/180, 20, 20, 300)
    #displayImage(lines_image,'lines image')

    #result
    print(lines_image.size)
    print(frame.size)
    result_image = weighted_img(lines_image,frame) #in case of error change to lines_image,frame
    #displayImage(result_image,'result image')
    cv2.imshow('result_image.jpg',result_image)
    
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

vc.release()
cv2.destroyAllWindows()

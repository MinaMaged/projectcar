# calibrate camera

import cv2
import numpy as np
from glob import glob
from os.path import isfile
import pickle
from scipy.misc import imresize, imread
from tqdm import tqdm
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

nX = 9
nY = 6
global gray_img


def displayImage(image, title):
    cv2.imshow(title, image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


def getCalibration():
    distp = pickle.load(open('CameraCalibration\camera_calibration_values.p', 'rb'))
    mtx = distp["mtx"]
    dist = distp["dist"]
    return mtx, dist


def calibrate_image():
    print('function started')
    object_points = []
    image_points = []
    object_p = np.zeros((6 * 9, 3), np.float32)
    object_p[:, :2] = np.mgrid[0:nX, 0:nY].T.reshape(-1, 2)
    # images = glob.glob('G:\CameraCalibration\calibration*')
    images = glob('CameraCalibration\calibration*')
    # gray = cv2.imread('CameraCalibration\calibration20.jpg')
    for img_name in images:
        print('entered for loop')
        img = cv2.imread(img_name)
        gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        # gray = gray_img
        ret, corners = cv2.findChessboardCorners(gray_img, (nX, nY), None)
        if ret == True:
            print('ret is true')
            image_points.append(corners)
            object_points.append(object_p)
            img = cv2.drawChessboardCorners(img, (nX, nY), corners, ret)
            # displayImage(img,'chess')

    ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(object_points, image_points, gray_img.shape[::-1], None, None)
    cameraCalFName = 'CameraCalibration\camera_calibration_values.p'
    output = open(cameraCalFName, 'wb')
    dictionary = {'mtx': 0, 'dist': 1}
    dictionary['mtx'] = mtx
    dictionary['dist'] = dist
    pickle.dump(dictionary, output)
    output.close()


def undistort(img):
    mtx, dist = getCalibration()
    undistorted = cv2.undistort(img, mtx, dist, None, mtx)
    return undistorted

import calibrateCamera
import cv2
import numpy as np
from os.path import isfile
from scipy.misc import imresize, imread
from imutils.video import FPS

# taken from internet to become variable not fixed
"""
offset = 250

prespective_src = np.float32([(132, 703),(540, 466),(740, 466),(1147, 703)])

prespective_dst = np.float32([
                    (prespective_src[0][0] + offset, 720),
                    (prespective_src[0][0] + offset, 0),
                    (prespective_src[-1][0] - offset, 0),
                    (prespective_src[-1][0] - offset, 720)])
"""


def getPrespective(img):
    image_size = (img.shape[1], img.shape[0])
    x = img.shape[1]
    y = img.shape[0]

    # the "order" of points in the polygon you are defining does not matter
    # but they need to match the corresponding points in destination_points!
    src1 = np.float32([
        [0.117 * x, y],
        [(0.5 * x) - (x * 0.078), (2 / 3) * y],
        [(0.5 * x) + (x * 0.078), (2 / 3) * y],
        [x - (0.117 * x), y]
    ])

    #     #chicago footage
    #     source_points = np.float32([
    #                 [300, 720],
    #                 [500, 600],
    #                 [700, 600],
    #                 [850, 720]
    #                 ])

    #     destination_points = np.float32([
    #                 [200, 720],
    #                 [200, 200],
    #                 [1000, 200],
    #                 [1000, 720]
    #                 ])

    dst1 = np.float32([
        [0.25 * x, y],
        [0.25 * x, 0],
        [x - (0.25 * x), 0],
        [x - (0.25 * x), y]
    ])
    src3 = np.float32(
        [[200, 720],
         [1100, 720],
         [595, 450],
         [685, 450]])
    dst3 = np.float32(
        [[300, 720],
         [980, 720],
         [300, 0],
         [980, 0]])
    src = np.float32([[585, 450], [203, 720], [1127, 720], [695, 450]])
    dst = np.float32([[320, 0], [320, 720], [960, 720], [960, 0]])
    src2 = np.float32([[580, 450], [160, x], [1150, x], [740, 450]])
    dst2 = np.float32([[0, 0], [0, x], [y, x], [y, 0]])

    h, w = img.shape[:2]

    # define source and destination points for transform
    src4 = np.float32([(575, 464),
                       (707, 464),
                       (258, 682),
                       (1049, 682)])
    dst4 = np.float32([(450, 0),
                       (w - 450, 0),
                       (450, h),
                       (w - 450, h)])

    perst = cv2.getPerspectiveTransform(src3, dst3)
    inv_perst = cv2.getPerspectiveTransform(dst3, src3)
    return cv2.warpPerspective(img, perst, image_size, flags=cv2.INTER_LINEAR), inv_perst


def abs_sobel_thresh(img, orient='x', thresh=(0, 255)):
    # Convert to grayscale
    gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    # Apply x or y gradient with the OpenCV Sobel() function
    # and take the absolute value
    if orient == 'x':
        abs_sobel = np.absolute(cv2.Sobel(gray, cv2.CV_64F, 1, 0))
    if orient == 'y':
        abs_sobel = np.absolute(cv2.Sobel(gray, cv2.CV_64F, 0, 1))
    # Rescale back to 8 bit integer
    scaled_sobel = np.uint8(255 * abs_sobel / np.max(abs_sobel))
    # Create a copy and apply the threshold
    binary_output = np.zeros_like(scaled_sobel)
    # Here I'm using inclusive (>=, <=) thresholds, but exclusive is ok too
    binary_output[(scaled_sobel >= thresh[0]) & (scaled_sobel <= thresh[1])] = 1

    # Return the result
    return binary_output


def mag_threshold(img, sobel_kernel=3, thresh=(0, 255)):
    # 1) Convert to grayscale
    gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    # 2) Take the gradient in x and y separately
    x = cv2.Sobel(gray, cv2.CV_64F, 1, 0, ksize=sobel_kernel)
    y = cv2.Sobel(gray, cv2.CV_64F, 0, 1, ksize=sobel_kernel)
    # 3) Calculate the xy magnitude 
    mag = np.sqrt(x ** 2 + y ** 2)
    # 4) Scale to 8-bit (0 - 255) and convert to type = np.uint8
    scale = np.max(mag) / 255
    eightbit = (mag / scale).astype(np.uint8)
    # 5) Create a binary mask where mag thresholds are met
    binary_output = np.zeros_like(eightbit)
    binary_output[(eightbit > thresh[0]) & (eightbit < thresh[1])] = 1
    return binary_output


def dir_threshold(img, sobel_kernel=3, thresh=(0, np.pi / 2)):
    # 1) Convert to grayscale
    gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    # 2) Take the gradient in x and y separately
    x = np.absolute(cv2.Sobel(gray, cv2.CV_64F, 1, 0, ksize=sobel_kernel))
    y = np.absolute(cv2.Sobel(gray, cv2.CV_64F, 0, 1, ksize=sobel_kernel))
    # 4) Use np.arctan2(abs_sobely, abs_sobelx) to calculate the direction of the gradient 
    direction = np.arctan2(y, x)
    binary_output = np.zeros_like(direction)
    binary_output[(direction > thresh[0]) & (direction < thresh[1])] = 1
    return binary_output


def hls_select(img, sthresh=(0, 255), lthresh=()):
    # 1) Convert to HLS color space
    hls_img = cv2.cvtColor(img, cv2.COLOR_RGB2HLS)
    # 2) Apply a threshold to the S channel
    L = hls_img[:, :, 1]
    S = hls_img[:, :, 2]
    # 3) Return a binary image of threshold result
    binary_output = np.zeros_like(S)
    binary_output[(S >= sthresh[0]) & (S <= sthresh[1])
                  & (L > lthresh[0]) & (L <= lthresh[1])] = 1
    return binary_output


def binary_pipeline(img):
    img_copy = cv2.GaussianBlur(img, (3, 3), 0)
    # img_copy = np.copy(img)

    # color channels
    s_binary = hls_select(img_copy, sthresh=(140, 255), lthresh=(120, 255))
    # red_binary = red_select(img_copy, thresh=(200,255))

    # Sobel x
    x_binary = abs_sobel_thresh(img_copy, thresh=(25, 200))
    y_binary = abs_sobel_thresh(img_copy, thresh=(25, 200), orient='y')
    xy = cv2.bitwise_and(x_binary, y_binary)

    # magnitude & direction
    mag_binary = mag_threshold(img_copy, sobel_kernel=3, thresh=(30, 100))
    dir_binary = dir_threshold(img_copy, sobel_kernel=3, thresh=(0.8, 1.2))

    # Stack each channel
    gradient = np.zeros_like(s_binary)
    gradient[((x_binary == 1) & (y_binary == 1)) | ((mag_binary == 1) & (dir_binary == 1))] = 1
    final_binary = cv2.bitwise_or(s_binary, gradient)

    return final_binary


def find_track_lanes(binary):
    global window
    histogram = np.sum(binary[int(binary.shape[0] / 2):, :], axis=0)
    out_image = np.dstack((binary, binary, binary)) * 255
    midpoint = np.int(histogram.shape[0] / 2)  # cannot understand 0 should be 1 21/8
    leftx = np.argmax(histogram[:midpoint])
    rightx = np.argmax(histogram[midpoint:]) + midpoint

    nwindows = 9  # because we need 1/9 of the screen
    window_height = np.int(binary.shape[0] / nwindows)
    # get x and y of all non zero pixels
    nonzeros = binary.nonzero()
    ynonzero = np.array(nonzeros[0])
    xnonzero = np.array(nonzeros[1])

    # we have to update positions for each frame
    leftx_current = leftx
    rightx_current = rightx

    # set width margin on +ve and -ve
    margin = 100  # to be tested
    # variable minimum to recenter window
    minpixels = 50
    # left and right matrices to store lane pixel indices
    leftlane_indices = []
    rightlane_indices = []

    for window in range(nwindows):
        y_low = int(binary.shape[0] - (window + 1) * window_height)
        y_high = int(binary.shape[0] - window * window_height)
        xleft_low = leftx_current - margin
        xleft_high = leftx_current + margin
        xright_low = rightx_current - margin
        xright_high = rightx_current + margin
        cv2.rectangle(out_image, (xleft_low, y_low), (xleft_high, y_high), (0, 255, 0), 3)
        cv2.rectangle(out_image, (xright_low, y_low), (xright_high, y_high), (0, 255, 0), 3)
        good_left_inds = \
        ((ynonzero >= y_low) & (ynonzero < y_high) & (xnonzero >= xleft_low) & (xnonzero < xleft_high)).nonzero()[0]
        good_right_inds = \
        ((ynonzero >= y_low) & (ynonzero < y_high) & (xnonzero >= xright_low) & (xnonzero < xright_high)).nonzero()[0]
        leftlane_indices.append(good_left_inds)
        rightlane_indices.append(good_right_inds)
        # if found pixels more than min recenter window on their mean
        if len(good_left_inds) > minpixels:
            leftx_current = np.int(np.mean(xnonzero[good_left_inds]))
        if len(good_right_inds) > minpixels:
            rightx_current = np.int(np.mean(xnonzero[good_right_inds]))

    # concatinate arrays of indices
    leftlane_indices = np.concatenate(leftlane_indices)
    rightlane_indices = np.concatenate(rightlane_indices)

    # Extract left and right line pixel positions
    leftx = xnonzero[leftlane_indices]
    lefty = ynonzero[leftlane_indices]
    rightx = xnonzero[rightlane_indices]
    righty = ynonzero[rightlane_indices]
    # fit 2nd order polynomial
    left_fit = np.polyfit(lefty, leftx, 2)
    # print rightx
    right_fit = np.polyfit(righty, rightx, 2)

    # create x and y values to plot them
    plot_y = np.linspace(0, binary.shape[0] - 1, binary.shape[0])
    left_fitx = left_fit[0] * plot_y ** 2 + left_fit[1] * plot_y + left_fit[2]
    right_fitx = right_fit[0] * plot_y ** 2 + right_fit[1] * plot_y + right_fit[2]

    nonzeros = binary.nonzero()
    ynonzero = np.array(nonzeros[0])
    xnonzero = np.array(nonzeros[1])
    margin = 100

    leftlane_indices = ((xnonzero > (left_fit[0] * (ynonzero ** 2) + left_fit[1] * ynonzero + left_fit[2] - margin)) & (
    xnonzero < (left_fit[0] * (ynonzero ** 2) + left_fit[1] * ynonzero + left_fit[2] + margin)))
    rightlane_indices = (
    (xnonzero > (right_fit[0] * (ynonzero ** 2) + right_fit[1] * ynonzero + right_fit[2] - margin)) & (
    xnonzero < (right_fit[0] * (ynonzero ** 2) + right_fit[1] * ynonzero + right_fit[2] + margin)))

    # Again, extract left and right line pixel positions
    leftx = xnonzero[leftlane_indices]
    lefty = ynonzero[leftlane_indices]
    rightx = xnonzero[rightlane_indices]
    righty = ynonzero[rightlane_indices]
    # Fit a second order polynomial to each
    left_fit = np.polyfit(lefty, leftx, 2)
    right_fit = np.polyfit(righty, rightx, 2)

    return left_fit, right_fit


def track_lanes_update(binary, left_fit, right_fit):
    global window_search
    global frame_count

    # for stability repeat window search
    if frame_count % 10 == 0:
        window_search = True

    nonzero = binary.nonzero()
    nonzeroy = np.array(nonzero[0])
    nonzerox = np.array(nonzero[1])
    margin = 100
    left_lane_inds = ((nonzerox > (left_fit[0] * (nonzeroy ** 2) + left_fit[1] * nonzeroy + left_fit[2] - margin)) & (
    nonzerox < (left_fit[0] * (nonzeroy ** 2) + left_fit[1] * nonzeroy + left_fit[2] + margin)))
    right_lane_inds = (
    (nonzerox > (right_fit[0] * (nonzeroy ** 2) + right_fit[1] * nonzeroy + right_fit[2] - margin)) & (
    nonzerox < (right_fit[0] * (nonzeroy ** 2) + right_fit[1] * nonzeroy + right_fit[2] + margin)))

    leftx = nonzerox[left_lane_inds]
    lefty = nonzeroy[left_lane_inds]
    rightx = nonzerox[right_lane_inds]
    righty = nonzeroy[right_lane_inds]
    # Fit a second order polynomial to each
    left_fit = np.polyfit(lefty, leftx, 2)
    right_fit = np.polyfit(righty, rightx, 2)
    # Generate x and y values for plotting
    ploty = np.linspace(0, binary.shape[0] - 1, binary.shape[0])
    left_fitx = left_fit[0] * ploty ** 2 + left_fit[1] * ploty + left_fit[2]
    right_fitx = right_fit[0] * ploty ** 2 + right_fit[1] * ploty + right_fit[2]

    return left_fit, right_fit, leftx, lefty, rightx, righty


def get_val(y, poly_coeff):
    return poly_coeff[0] * y ** 2 + poly_coeff[1] * y + poly_coeff[2]


def lane_fill_poly(binary, undist, left_fit, right_fit):
    # Generate x and y values
    ploty = np.linspace(0, binary.shape[0] - 1, binary.shape[0])
    left_fitx = get_val(ploty, left_fit)
    right_fitx = get_val(ploty, right_fit)

    # Create an image to draw the lines on
    warp_zero = np.zeros_like(binary).astype(np.uint8)
    color_warp = np.dstack((warp_zero, warp_zero, warp_zero))

    # Recast x and y for cv2.fillPoly()
    pts_left = np.array([np.transpose(np.vstack([left_fitx, ploty]))])
    pts_right = np.array([np.flipud(np.transpose(np.vstack([right_fitx, ploty])))])
    pts = np.hstack((pts_left, pts_right))

    # Draw the lane 
    cv2.fillPoly(color_warp, np.int_([pts]), (0, 255, 0))
    bird_eye_view, inverse_transform = getPrespective(binary_image)
    # Warp using inverse perspective transform
    newwarp = cv2.warpPerspective(color_warp, inverse_transform, (binary.shape[1], binary.shape[0]))
    # overlay
    # newwarp = cv.cvtColor(newwarp, cv.COLOR_BGR2RGB)
    result = cv2.addWeighted(undist, 1, newwarp, 0.3, 0)

    return result


def measure_curve(binary_warped, left_fit, right_fit):
    # generate y values
    ploty = np.linspace(0, binary_warped.shape[0] - 1, binary_warped.shape[0])

    # measure radius at the maximum y value, or bottom of the image
    # this is closest to the car 
    y_eval = np.max(ploty)

    # coversion rates for pixels to metric
    # THIS RATE CAN CHANGE GIVEN THE RESOLUTION OF THE CAMERA!!!!!
    # BE SURE TO CHANGE THIS IF USING DIFFERENT SIZE IMAGES!!!
    ym_per_pix = 30 / 720  # meters per pixel in y dimension
    xm_per_pix = 3.7 / 700  # meters per pixel in x dimension

    # x positions lanes
    # leftx = get_val(ploty,left_fit)
    # rightx = get_val(ploty,right_fit)

    # fit polynomials in metric 
    # left_fit_cr = np.polyfit(ploty*ym_per_pix, leftx*xm_per_pix, 2)
    # right_fit_cr = np.polyfit(ploty*ym_per_pix, rightx*xm_per_pix, 2)

    # calculate radii in metric from radius of curvature formula
    # left_curverad = ((1 + (2*left_fit_cr[0]*y_eval*ym_per_pix + left_fit_cr[1])**2)**1.5) / np.absolute(2*left_fit_cr[0])
    # right_curverad = ((1 + (2*right_fit_cr[0]*y_eval*ym_per_pix + right_fit_cr[1])**2)**1.5) / np.absolute(2*right_fit_cr[0])
    # center = (((left_fit[0]*720**2+left_fit[1]*720+left_fit[2]) +(right_fit[0]*720**2+right_fit[1]*720+right_fit[2]) ) /2 - 640)*xm_per_pix
    left_curverad = ((1 + (2 * left_fit[0] * y_eval * ym_per_pix + left_fit[1]) ** 2) ** 1.5) / np.absolute(
        2 * left_fit[0])
    right_curverad = ((1 + (2 * right_fit[0] * y_eval * ym_per_pix + right_fit[1]) ** 2) ** 1.5) / np.absolute(
        2 * right_fit[0])
    center = (((left_fit[0] * 720 ** 2 + left_fit[1] * 720 + left_fit[2]) + (
    right_fit[0] * 720 ** 2 + right_fit[1] * 720 + right_fit[2])) / 2 - 640) * xm_per_pix
    return left_curverad, right_curverad, center


def vehicle_offset(img, left_fit, right_fit):
    # THIS RATE CAN CHANGE GIVEN THE RESOLUTION OF THE CAMERA!!!!!
    # BE SURE TO CHANGE THIS IF USING DIFFERENT SIZE IMAGES!!!
    xm_per_pix = 3.7 / 700
    image_center = img.shape[1] / 2

    ## find where lines hit the bottom of the image, closest to the car
    left_low = get_val(img.shape[0], left_fit)
    right_low = get_val(img.shape[0], right_fit)

    # pixel coordinate for center of lane
    lane_center = (left_low + right_low) / 2.0

    ## vehicle offset
    distance = image_center - lane_center

    ## convert to metric
    return (round(distance * xm_per_pix, 5))


# Main code
cv2.ocl.haveOpenCL()
cv2.ocl.setUseOpenCL(True)
print(cv2.ocl.haveOpenCL())
if isfile('CameraCalibration\camera_calibration_values.p'):
    calibrateCamera.getCalibration()
    print("file found")
else:
    calibrateCamera.calibrate_image()

vc = cv2.VideoCapture('project_video.mp4')

fps = FPS().start()
# img = cv2.imwrite('image.jpg',img) #to save image
# displayImage(img,'Original')

if vc.isOpened():
    rval = True
    print('true')
else:
    rval = False
    print('false')

fourcc = cv2.VideoWriter_fourcc(*'XVID')
out = cv2.VideoWriter('output1.avi', fourcc, 25.0, (1280, 720))

global window_search
window_search = True
global left_fit_prev
global right_fit_prev
global frame_count
frame_count = 0
global curve_radius
global offset
lane_found = False
while rval == True:
    rval, frame = vc.read()
    if rval == False:
        break

    image = calibrateCamera.undistort(frame)
    binary_image = binary_pipeline(image)
    bird_eye_view, inverse_transform = getPrespective(binary_image)
    # left_fit , right_fit = find_track_lanes(bird_eye_view)
    # global frame_count
    # frame_count=0
    # left_fit,right_fit,leftx,lefty,rightx,righty = track_lanes_update(bird_eye_view, left_fit,right_fit)
    # final_lane = lane_fill_poly(bird_eye_view,image,left_fit,right_fit)
    # left_radius,right_radius,center = measure_curve(bird_eye_view,left_fit,right_fit)
    # print("left line radius = {:.2f}".format(left_radius))
    # print("right line radius = {:.2f}".format(right_radius))
    try:
        if window_search:
            window_search = False
            # window search
            left_fit, right_fit = find_track_lanes(bird_eye_view)
            # store values
            left_fit_prev = left_fit
            right_fit_prev = right_fit
            lane_found = True
        else:
            # load values
            left_fit = left_fit_prev
            right_fit = right_fit_prev
            # search in margin of polynomials
            left_fit, right_fit, leftx, lefty, rightx, righty = track_lanes_update(bird_eye_view, left_fit, right_fit)
            lane_found = True
        # save values
        left_fit_prev = left_fit
        right_fit_prev = right_fit

        # draw polygon
        processed_frame = lane_fill_poly(bird_eye_view, image, left_fit, right_fit)

        # update ~twice per second
        if frame_count == 0 or frame_count % 15 == 0:
            # measure radii
            left_curve, right_curve, center = measure_curve(bird_eye_view, left_fit, right_fit)
            # measure offset
            offset = vehicle_offset(image, left_fit, right_fit)
        font = cv2.FONT_HERSHEY_TRIPLEX
        processed_frame = cv2.putText(processed_frame,
                                      'Radius Left: ' + str(float("{0:.3f}".format(left_curve))) + ' m', (30, 40), font,
                                      1, (255, 255, 255), 2)
        processed_frame = cv2.putText(processed_frame,
                                      'Radius Right: ' + str(float("{0:.3f}".format(right_curve))) + ' m', (30, 80),
                                      font, 1, (255, 255, 255), 2)
        processed_frame = cv2.putText(processed_frame, 'Vehicle offset: ' + str(float("{0:.2f}".format(offset))) + ' m',
                                      (30, 120), font, 1, (255, 255, 255), 2)
        processed_frame = cv2.putText(processed_frame, 'Lane detected!', (30, 160), font, 1, (0, 255, 0), 2)
    except:
        lane_found = False
        processed_frame = cv2.putText(image, 'Lane not detected!', (30, 160), font, 1, (0, 0, 255), 2)
    # cv2.imshow('final',processed_frame)
    out.write(processed_frame)
    frame_count += 1
    fps.update()
    """
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
    """

fps.stop()
print("[INFO] elasped time: {:.2f}".format(fps.elapsed()))
print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))
cv2.destroyAllWindows()
vc.release()
out.release()
